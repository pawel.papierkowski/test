package com.mytest.bruh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BruhApplication {
	public static void main(String[] args) {
		SpringApplication.run(BruhApplication.class, args);
	}
}