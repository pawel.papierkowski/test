package com.mytest.bruh.api;

import com.mytest.bruh.services.basic.HealthResp;
import com.mytest.bruh.services.basic.HealthService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Reports status of server.
 */
@RestController
@RequestMapping(path = "/api/check")
@RequiredArgsConstructor
public class CheckApi {
    private final HealthService healthService;

    @GetMapping("/health")
    public HealthResp getHealth() {
        return healthService.getStatus();
    }
}