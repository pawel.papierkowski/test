package com.mytest.bruh.api;

import com.mytest.bruh.services.BruhService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * API example.
 */
@RestController
@RequestMapping(path = "/api/bruh")
@RequiredArgsConstructor
public class BruhApi {
    private final BruhService bruhService;
}