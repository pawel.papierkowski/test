package com.mytest.bruh.services;

import org.springframework.core.SpringVersion;
import org.springframework.stereotype.Service;

@Service
public class BruhService {
    public String getSpringVersion() {
        return SpringVersion.getVersion();
    }
}