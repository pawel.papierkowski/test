package com.mytest.bruh.services.basic;

import org.springframework.core.SpringVersion;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class HealthService {
    private AtomicLong counter = new AtomicLong();
    LocalDateTime upAt = ZonedDateTime.now().withZoneSameInstant(ZoneId.of("Europe/Warsaw")).toLocalDateTime();

    public HealthResp getStatus() {
        ZonedDateTime nowZoned = ZonedDateTime.now();
        nowZoned.withZoneSameInstant(ZoneId.of("Europe/Warsaw")); // move zone from UTC to Poland
        LocalDateTime nowAt = nowZoned.toLocalDateTime();

        return HealthResp.builder()
                .serverUp(upAt)
                .serverTime(nowAt)
                .springVersion(SpringVersion.getVersion())
                .counter(counter.incrementAndGet())
                .build();
    }
}
