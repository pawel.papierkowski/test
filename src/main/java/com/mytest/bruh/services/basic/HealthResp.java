package com.mytest.bruh.services.basic;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;

import java.time.LocalDateTime;

@Value
@Builder(toBuilder = true)
@JsonDeserialize(builder = HealthResp.HealthRespBuilder.class)
public class HealthResp {
    LocalDateTime serverUp;
    LocalDateTime serverTime;
    String springVersion;
    Long counter;

    @JsonPOJOBuilder(withPrefix = "")
    public static class HealthRespBuilder {
        LocalDateTime serverUp;
        LocalDateTime serverTime;
        String springVersion;
        Long counter;
    }
}